## Description
Precor Connect extended warranty service SDK for javascript.


## Setup

**install via jspm**  
```shell
jspm install extended-warranty-service-sdk=bitbucket:precorconnect/extended-warranty-service-sdk-for-javascript
```

**import & instantiate**
```javascript
import ExtendedWarrantyServiceSdk,{ExtendedWarrantyServiceSdkConfig} from 'extended-warranty-service-sdk'

const extendedWarrantyServiceSdkConfig =
    new ExtendedWarrantyServiceSdkConfig(
        "https://api-dev.precorconnect.com"
    );
    
const extendedWarrantyServiceSdk =
    new ExtendedWarrantyServiceSdk(
        extendedWarrantyServiceSdkConfig
    );
```

## Platform Support

This library can be used in the **browser**.

## Develop

#### Software
- git
- npm

#### Scripts

install dependencies (perform prior to running or testing locally)
```PowerShell
npm install
```

unit & integration test in multiple browsers/platforms
```PowerShell
# note: following environment variables must be present:
# SAUCE_USERNAME
# SAUCE_ACCESS_KEY
npm test
```