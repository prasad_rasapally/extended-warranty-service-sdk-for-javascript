import PartnerSaleRegDraftSaleSimpleLineItem from './partnerSaleRegDraftSaleSimpleLineItem';
import PartnerSaleRegDraftSaleCompositeLineItem from './partnerSaleRegDraftSaleCompositeLineItem';
import ExtendedWarrantyDraftRes from './extendedWarrantyDraftRes';
import PartnerSaleRegDraftSaleCompositeLineItemFactory from './partnerSaleRegDraftSaleCompositeLineItemFactory';
import PartnerSaleRegDraftSaleSimpleLineItemFactory from './partnerSaleRegDraftSaleSimpleLineItemFactory';

/**
 * @class {GetExtendedWarrantyPurchaseFactory}
 */
export default class GetExtendedWarrantyPurchaseFactory {

    static construct(data):ExtendedWarrantyDraftRes {

        var obj = {};
        obj.partnerSaleRegistrationId = data.partnerSaleRegistrationId;

        obj.facilityName = data.facilityName;
        obj.partnerAccountId = data.partnerAccountId;
        obj.isSubmitted = data.submitted;

        if(obj.discountCode){
            obj.discountCode = data.discountCode;
        }
        if(data.compositeLineItems) {
            obj.compositeLineItems =
                PartnerSaleRegDraftSaleCompositeLineItemFactory
                    .construct(
                        data.compositeLineItems
                    );
        }
        if (data.simpleLineItems){
            obj.simpleLineItems =
                PartnerSaleRegDraftSaleSimpleLineItemFactory
                    .construct(
                        data.simpleLineItems
                    );
        }
        if(data.totalPrice){
            obj.totalPrice = data.totalPrice;
        }
        if(data.discountAppliedPrice){
            obj.discountAppliedPrice = data.discountAppliedPrice;
        }


        obj.purchaseOrder = data.purchaseOrder;
        obj.sapAccountNumber = data.sapAccountNumber;


        return obj;
    }
}
