/**
 * @class {ExtendedWarrantyServiceSdkConfig}
 */
export default class ExtendedWarrantyServiceSdkConfig {

    _precorConnectApiBaseUrl:string;

    /**
     * @param {string} precorConnectApiBaseUrl
     */
    constructor(precorConnectApiBaseUrl:string) {

        if (!precorConnectApiBaseUrl) {
            throw 'precorConnectApiBaseUrl required';
        }
        this._precorConnectApiBaseUrl = precorConnectApiBaseUrl;

    }

    /**
     * @returns {string}
     */
    get precorConnectApiBaseUrl():string {
        return this._precorConnectApiBaseUrl;
    }

}