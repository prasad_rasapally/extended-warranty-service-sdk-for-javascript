import {inject} from 'aurelia-dependency-injection';
import {HttpClient} from 'aurelia-http-client';
import ExtendedWarrantySubmitReq from './extendedWarrantySubmitReq';
import ExtendedWarrantyDraftRes from './extendedWarrantyDraftRes';
import ExtendedWarrantyServiceSdkConfig from './extendedWarrantyServiceSdkConfig';

/**
 * @class {SubmitExtendedWarrantyDraftFeature}
 */
@inject(ExtendedWarrantyServiceSdkConfig, HttpClient)
class SubmitExtendedWarrantyDraftFeature {

    _config:ExtendedWarrantyServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:ExtendedWarrantyServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     * @param request {ExtendedWarrantySubmitReq}
     * @param accessToken {string}
     * @returns {Promise.<>}
     */
    execute(request:ExtendedWarrantySubmitReq,
            accessToken:string):Promise<ExtendedWarrantyDraftRes> {

        return this._httpClient
            .createRequest('extended-warranty/submitExtendedWarrantyDraft')
            .asPost()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withContent(request)
            .send()
            .then(response => response.content);

    }

}
export default SubmitExtendedWarrantyDraftFeature;

