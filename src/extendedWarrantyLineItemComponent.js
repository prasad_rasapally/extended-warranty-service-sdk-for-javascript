/**
 * @class {ExtendedWarrantyLineItemComponent}
 */
export default class ExtendedWarrantyLineItemComponent{

    _id:number;

    _assetId:string;

    _serialNumber:string;

    _productLineName:string;

    constructor(id:number,
                assetId:string,
                serialNumber:string,
                productLineName:string
    ){
        this._id = id;

        if(!assetId){
            throw new TypeError('assetId required');
        }
        this._assetId = assetId;

        if(!serialNumber){
            throw new TypeError('serialNumber required');
        }
        this._assetId = assetId;

        this._productLineName = productLineName;

    }

    /**
     * @returns {string}
     */
    get id():number{
        return this._id;
    }

    get assetId():string{
        return this._assetId;
    }

    get serialNumber():string{
        return this._serialNumber;
    }

    get productLineName():string{
        return this._productLineName;
    }

    toJSON(){
        return {
            id:this._id,
            assetId:this._assetId,
            serialNumber:this._serialNumber,
            productLineName:this._productLineName
        }
    }
}
