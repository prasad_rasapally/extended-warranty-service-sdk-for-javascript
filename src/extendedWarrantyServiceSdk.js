import ExtendedWarrantyServiceSdkConfig from './extendedWarrantyServiceSdkConfig';
import DiContainer from './diContainer';
import AddExtendedWarrantyDraftReq from './addExtendedWarrantyDraftReq';
import AddExtendedWarrantyPurchaseFeature from './addExtendedWarrantyPurchaseFeature';
import PartnerCommercialSaleRegSynopsisView from './partnerCommercialSaleRegSynopsisView';
import ListPartnerSaleRegDraftsWithPartnerAccountIdFeature from './listPartnerSaleRegDraftsWithPartnerAccountIdFeature';
import ExtendedWarrantyDraftRes from './extendedWarrantyDraftRes';
import GetExtendedWarrantyPurchaseFeature from './getExtendedWarrantyPurchaseFeature';
import CheckDiscountAvailOnExtendedWarrantyFeature from './checkDiscountAvailOnExtendedWarrantyFeature';
import ExtendedWarrantySubmitReq from './extendedWarrantySubmitReq';
import UpdateExtendedWarrantyDraftReq from './updateExtendedWarrantyDraftReq';
import SubmitExtendedWarrantyDraftFeature from './submitExtendedWarrantyDraftFeature';
import UpdateExtendedWarrantyDraftFeature from './updateExtendedWarrantyDraftFeature';
import ExtendedWarrantyId from './extendedWarrantyId';

/**
 * @class {ExtendedWarrantyServiceSdk}
 */
export default class ExtendedWarrantyServiceSdk {

    _diContainer:DiContainer;

    /**
     * @param {ExtendedWarrantyServiceSdkConfig} config
     */
    constructor(config:ExtendedWarrantyServiceSdkConfig) {

        this._diContainer = new DiContainer(config);

    }

    /**
     * @param {string} partnerAccountId
     * @param {string} accessToken
     * @returns {PartnerCommercialSaleRegSynopsisView[]}
     */
    listSubmittedPartnerCommercialSaleRegDraftWithAccountId(
        partnerAccountId:string,
        accessToken:string):Promise<PartnerCommercialSaleRegSynopsisView[]> {

        return this
            ._diContainer
            .get(ListPartnerSaleRegDraftsWithPartnerAccountIdFeature)
            .execute(
                partnerAccountId,
                accessToken
            );
    }

    /**
     *
     * @param {AddExtendedWarrantyDraftReq} request
     * @param {string} accessToken
     * @returns {Promise.<ExtendedWarrantyId>}
     */
    addExtendedWarrantyPurchase(
        request:AddExtendedWarrantyDraftReq,
        accessToken:string):Promise<ExtendedWarrantyId>{

        return this
            ._diContainer
            .get(AddExtendedWarrantyPurchaseFeature)
            .execute(
                request,
                accessToken
            );
    }

    /**
     * @param {string} discountCode
     * @param {string} accessToken
     * @returns {boolean}
     */
    checkDiscountAvailOnExtendedWarranty(
        discountCode:string,
        accessToken:string):Promise<boolean> {

        return this
            ._diContainer
            .get(CheckDiscountAvailOnExtendedWarrantyFeature)
            .execute(
                discountCode,
                accessToken
            );

    }

    /**
     * @param extendedWarrantyId
     * @param {UpdateExtendedWarrantyDraftReq} request
     * @param {string} accessToken
     * @returns {ExtendedWarrantyId}
     */
    updateExtendedWarrantyPurchase(
        extendedWarrantyId:string,
        request:UpdateExtendedWarrantyDraftReq,
        accessToken:string):Promise<ExtendedWarrantyId> {

        return this
            ._diContainer
            .get(UpdateExtendedWarrantyDraftFeature)
            .execute(
                extendedWarrantyId,
                request,
                accessToken
            );
    }

    /**
     * @param {ExtendedWarrantySubmitReq} request
     * @param {string} accessToken
     * @returns {ExtendedWarrantyDraftRes}
     */
    submitExtendedWarrantyDraft(
        request:ExtendedWarrantySubmitReq,
        accessToken:string):Promise<ExtendedWarrantyDraftRes> {

        return this
            ._diContainer
            .get(SubmitExtendedWarrantyDraftFeature)
            .execute(
                request,
                accessToken
            )
    }

    /**
     * @param {number} partnerSaleRegistrationId
     * @param {string} accessToken
     * @returns {ExtendedWarrantyDraftRes}
     */
    getExtendedWarrantyPurchase(
        partnerSaleRegistrationId:number,
        accessToken:string):Promise<ExtendedWarrantyDraftRes> {

        return this
            ._diContainer
            .get(GetExtendedWarrantyPurchaseFeature)
            .execute(
                partnerSaleRegistrationId,
                accessToken
            );
    }

}
