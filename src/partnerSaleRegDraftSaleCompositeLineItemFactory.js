import PartnerSaleRegDraftSaleCompositeLineItem from './partnerSaleRegDraftSaleCompositeLineItem';
import PartnerSaleLineItemComponentFactory from './partnerSaleLineItemComponentFactory';

export default class PartnerSaleRegDraftSaleCompositeLineItemFactory {

    static construct(data):PartnerSaleRegDraftSaleCompositeLineItem[] {

        var componentsObj = [];

        Array.from(
            data,
            dataItem => {
                var components = {};

                if(dataItem.terms){
                    components.terms = dataItem.terms;
                    components.selectedPrice = dataItem.selectedPrice;
                }
                if(dataItem.price){
                    components.price = dataItem.price;
                }
                if(dataItem.selectedTerms){
                    components.selectedTerms = dataItem.selectedTerms;
                }

                if(dataItem.materialNumber){
                    components.materialNumber = dataItem.materialNumber;
                }

                components.components =
                    PartnerSaleLineItemComponentFactory
                        .construct(
                            dataItem.components
                        );
                componentsObj.push(components);
            });

        return componentsObj;

    }
}
