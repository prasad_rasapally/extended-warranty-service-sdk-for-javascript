import PartnerSaleRegDraftSaleSimpleLineItem from './partnerSaleRegDraftSaleSimpleLineItem';
import PartnerSaleRegDraftSaleCompositeLineItem from './partnerSaleRegDraftSaleCompositeLineItem';
import PartnerCommercialSaleRegSynopsisView from './partnerCommercialSaleRegSynopsisView';
import PartnerSaleRegDraftSaleCompositeLineItemFactory from './partnerSaleRegDraftSaleCompositeLineItemFactory';
import PartnerSaleRegDraftSaleSimpleLineItemFactory from './partnerSaleRegDraftSaleSimpleLineItemFactory';

export default class ListSubmittedPartnerCommercialSaleRegDraftWithAccountIdFactory {

    static construct(data):PartnerCommercialSaleRegSynopsisView {

        var obj = {};
        obj.id = data.id;
        obj.facilityName = data.facilityName;
        obj.partnerAccountId = data.partnerAccountId;
        obj.installDate = data.installDate;
        obj.draftSubmittedDate = data.draftSubmittedDate;

        if(data.partnerRepUserId) {
            obj.partnerRepUserId = data.partnerRepUserId;
        }

        if(data.compositeLineItems) {
            obj.compositeLineItems =
                PartnerSaleRegDraftSaleCompositeLineItemFactory
                    .construct(
                        data.compositeLineItems
                    );
        }

        if (data.simpleLineItems){
            obj.simpleLineItems =
                PartnerSaleRegDraftSaleSimpleLineItemFactory
                    .construct(
                        data.simpleLineItems
                    );
        }

        return obj;
    }
}
