import {inject} from 'aurelia-dependency-injection';
import {HttpClient} from 'aurelia-http-client';
import UpdateExtendedWarrantyDraftReq from './updateExtendedWarrantyDraftReq';
import ExtendedWarrantyServiceSdkConfig from './extendedWarrantyServiceSdkConfig';
import ExtendedWarrantyId from './extendedWarrantyId';

@inject(ExtendedWarrantyServiceSdkConfig, HttpClient)
class UpdateExtendedWarrantyDraftFeature {

    _config:ExtendedWarrantyServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:ExtendedWarrantyServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     * @param {string} extendedWarrantyId
     * @param {UpdateExtendedWarrantyDraftReq} request
     * @param {string} accessToken
     * @returns {Promise<ExtendedWarrantyId>}
     */
    execute(extendedWarrantyId:string,
            request:UpdateExtendedWarrantyDraftReq,
            accessToken:string):Promise<ExtendedWarrantyId> {

        return this._httpClient
            .createRequest(`extended-warranty/updateExtendedWarrantyDraft/${extendedWarrantyId}`)
            .asPost()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withContent(request)
            .send()
            .then(response => response.content);
    }
}

export default UpdateExtendedWarrantyDraftFeature;
