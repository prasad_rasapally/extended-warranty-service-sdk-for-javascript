import {inject} from 'aurelia-dependency-injection';
import {HttpClient} from 'aurelia-http-client';
import PartnerCommercialSaleRegSynopsisView from './partnerCommercialSaleRegSynopsisView';
import ExtendedWarrantyServiceSdkConfig from './extendedWarrantyServiceSdkConfig';
import ListSubmittedPartnerCommercialSaleRegDraftWithAccountIdFactory from './listSubmittedPartnerCommercialSaleRegDraftWithAccountIdFactory';

@inject(ExtendedWarrantyServiceSdkConfig, HttpClient)
class ListPartnerSaleRegDraftsWithPartnerAccountIdFeature {

    _config:ExtendedWarrantyServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:ExtendedWarrantyServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    execute(partnerAccountId:string,
            accessToken:string):Promise<PartnerCommercialSaleRegSynopsisView[]> {

        return this._httpClient
            .createRequest(`extended-warranty/submittedDrafts/accountId/${partnerAccountId}`)
            .asGet()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .send()
            .then(response =>
                Array.from(
                    response.content,
                    contentItem => ListSubmittedPartnerCommercialSaleRegDraftWithAccountIdFactory.construct(contentItem)
                )
            );
    }
}

export default ListPartnerSaleRegDraftsWithPartnerAccountIdFeature;
