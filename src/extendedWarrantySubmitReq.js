/**
 * @class {ExtendedWarrantySubmitReq}
 */
export default class ExtendedWarrantySubmitReq {


    _extendedWarrantyId:string;

    _partnerSaleRegistrationId:number;

    _purchaseOrder:string;

    _partnerAccountId:string;

    _sapAccountNumber:string;

    _totalPrice:number;

    _discountCode:string;

    _discountAppliedPrice:number;

    /**
     * @param extendedWarrantyId
     * @param partnerSaleRegistrationId
     * @param purchaseOrder
     * @param partnerAccountId
     * @param sapAccountNumber
     * @param totalPrice
     * @param discountCode
     * @param discountAppliedPrice
     */
    constructor(
        extendedWarrantyId:string,
        partnerSaleRegistrationId:number,
        purchaseOrder:string,
        partnerAccountId:string,
        sapAccountNumber:string,
        totalPrice:number,
        discountCode:string,
        discountAppliedPrice:number
    ) {

        if(!extendedWarrantyId){
            throw new TypeError('extendedWarrantyId required');
        }
        this._extendedWarrantyId = extendedWarrantyId;

        if(!partnerSaleRegistrationId){
            throw new TypeError('partnerSaleRegistrationId required');
        }
        this._partnerSaleRegistrationId = partnerSaleRegistrationId;

        if(!purchaseOrder){
            throw new TypeError('purchaseOrder required');
        }
        this._purchaseOrder = purchaseOrder;

        if(!partnerAccountId){
            throw new TypeError('partnerAccountId required');
        }
        this._partnerAccountId = partnerAccountId;

        if(!sapAccountNumber){
            throw new TypeError('sapAccountNumber required');
        }
        this._sapAccountNumber = sapAccountNumber;

        if(!totalPrice){
            throw new TypeError('totalPrice required');
        }
        this._totalPrice = totalPrice;

        this._discountCode = discountCode;

        this._discountAppliedPrice = discountAppliedPrice;

    }

    /**
     * @returns {string}
     */
    get extendedWarrantyId():string {
        return this._extendedWarrantyId;
    }

    /**
     * @returns {number}
     */
    get partnerSaleRegistrationId():number {
        return this._partnerSaleRegistrationId;
    }

    /**
     * @returns {string}
     */
    get purchaseOrder():string {
        return this._purchaseOrder;
    }

    /**
     * @returns {string}
     */
    get partnerAccountId():string {
        return this._partnerAccountId;
    }

    /**
     * @returns {string}
     */
    get sapAccountNumber():string {
        return this._sapAccountNumber;
    }

    /**
     * @returns {number}
     */
    get totalPrice():number {
        return this._totalPrice;
    }

    /**
     * @returns {string}
     */
    get discountCode():string {
        return this._discountCode;
    }

    /**
     * @returns {number}
     */
    get discountAppliedPrice():number {
        return this._discountAppliedPrice;
    }

    toJSON() {
        return {
            extendedWarrantyId: this._extendedWarrantyId,
            partnerSaleRegistrationId: this._partnerSaleRegistrationId,
            purchaseOrder: this._purchaseOrder,
            partnerAccountId: this._partnerAccountId,
            sapAccountNumber: this._sapAccountNumber,
            totalPrice: this._totalPrice,
            discountCode: this._discountCode,
            discountAppliedPrice: this._discountAppliedPrice
        }
    }

}
